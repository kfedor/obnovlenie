#ifndef OConfig_H
#define OConfig_H

#include <QSettings>
#include <QDebug>

#define O_CONFIG "obnovlenie.ini"

class OConfig
{
public:
    bool loadSettings();
    QString getProgramName() { return program_name; }
    QString getRemotePath() { return remote_path; }
    QString getURL() { return url; }
    QString getLogin() { return login; }
    QString getPassword() { return password; }

private:
    QString program_name;
    QString url;
    QString remote_path;
    QString login;
    QString password;

    QString chopString( QString in );
};

#endif // POSConfig_H
