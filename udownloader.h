#ifndef UDOWNLOADER_H
#define UDOWNLOADER_H

#include <QObject>
#include <QUrl>
#include <QFile>
#include <QFileInfo>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QDebug>
#include <QSslError>
#include <QList>
#include <QDir>

/*
#define URL "ftp://192.168.0.110"
#define REMOTE_PATH "/home/fedor/FTP/foto"
*/
#define LOCAL_PATH "tmp_download"

class UDownloader : public QObject
{
    Q_OBJECT
    QNetworkAccessManager nmanager;
    QList<QNetworkReply *> currentDownloads;

public:
    explicit UDownloader(QObject *parent = nullptr);
    void setup( QString, QString, QString, QString );
    void downloadFile( QString );

signals:    
    void fileComplete( QString );
    void fileError( QString );

public slots:
    void downloadFinished( QNetworkReply * );
    void sslErrors( const QList<QSslError> &sslErrors );
    void slotError( QNetworkReply::NetworkError );

private:
    QString url;
    QString remote_path;
    QString login;
    QString password;

    QString saveFileName( const QUrl &url );
    bool saveToDisk( const QString &filename, QIODevice *data );
};

#endif // UDOWNLOADER_H
