#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    downloaded_count = 0;

    setWindowTitle( QString("Менеджер обновлений v.%1").arg( VERSION ) );

    if( oconfig.loadSettings() == false )
    {
        addLogWindow( "Ошибка загрузки файла конфигурации обновления" );
        finishProgramWithError();
        return;
    }

    ud.setup( oconfig.getURL(), oconfig.getRemotePath(), oconfig.getLogin(), oconfig.getPassword() );

    connect( &ud, SIGNAL( fileError(QString) ), this, SLOT( fileError(QString) ) );
    connect( &ud, SIGNAL( fileComplete(QString) ) , this, SLOT( fileComplete(QString) ) );

    ui->pushButton->setEnabled( false );

    ui->label_2->setText("Скачиваем файл индекса");
    ud.downloadFile( INDEX_JSON );        
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addLogWindow( QString str )
{
    ui->plainTextEdit->appendPlainText( str );
}

void MainWindow::parseIndexJson()
{
    QJsonDocument root_doc = loadJson();

    if( root_doc.isNull() )
    {
        addLogWindow("Ошибка открытия index.json");
        return;
    }

    QJsonObject root_obj = root_doc.object();
    QJsonArray root_arr = root_obj["files"].toArray();

    if( root_arr.size() == 0 )
    {
        addLogWindow("В каталоге нет файлов");
        return;
    }

    foreach( const QJsonValue &value, root_arr )
    {
        QJsonObject obj = value.toObject();
        QString name = obj["name"].toString();
        QString sha1sum = obj["sha1sum"].toString();

        //qDebug() << name;
        //qDebug() << sha1sum;

        QString local_hash = getFileSHA1( name );
        ui->label_2->setText( QString("Проверяем файл %1").arg( name ) );

        if( local_hash != sha1sum )
        {
            //addLogWindow( QString("Добавляем файл %1").arg( name ) );

            downloadData item;
            item.name = name;
            item.sha1sum = sha1sum;
            dlData.append( item );
        }
        else
        {
            addLogWindow( QString("Пропускаем файл %1").arg( name ) );
        }
    }

    // начинаем качать с первого в списке
    if( dlData.size() > 0 )
    {
        processDownloadItem( 0 );
    }
    else
    {
        addLogWindow( "Обновлений нет" );
        finishProgram();
    }
}

void MainWindow::processDownloadItem( quint32 item_no )
{
    QString logtext = QString("Скачиваем файл %1").arg( dlData[item_no].name );
    addLogWindow( logtext );
    ui->label_2->setText( logtext );
    ud.downloadFile( dlData[item_no].name );
}

void MainWindow::fileError( QString msg )
{
    addLogWindow( msg );
    finishProgramWithError();
}

void MainWindow::fileComplete( QString file )
{
    if( file.endsWith( INDEX_JSON ) )
    {
        addLogWindow("Формируем список обновляемых файлов");
        ui->label_2->setText( "Формируем список обновляемых файлов" );
        parseIndexJson();        
    }
    else
    {
        QString sha1sum = getFileSHA1( file );

        if( sha1sum == dlData[downloaded_count].sha1sum )
        {
            addLogWindow( "Успешно скачали файл " + file );
            setProgress( ++downloaded_count, dlData.size() );

            if( downloaded_count == dlData.size() )
            {
                addLogWindow( "Скачивание завершено" );

                // копировать файлы
                moveDownloadedFiles();
            }
            else
                processDownloadItem( downloaded_count );
        }
        else
        {
            // error fatal
            addLogWindow( "При скачивании файла " + file + " произошла ошибка, обновление прервано" );
            finishProgramWithError();
        }
    }
}

void MainWindow::moveDownloadedFiles()
{
    QDir dir = QDir::currentPath();

    for( auto i: dlData )
    {
        QString old_path = QString(LOCAL_PATH) + "/" + i.name;
        addLogWindow( QString( "Перемещаем файл %1 в %2" ).arg( old_path ).arg( dir.currentPath() ) );

        QString path = QFileInfo( i.name ).path();

        if( !dir.exists( path ) )
            dir.mkpath( path );

        if( dir.exists( i.name ) )
        {
            if( !dir.remove( i.name ) )
            {
                addLogWindow( QString("Ошибка удаления старого файла %1").arg( i.name ) );
                finishProgramWithError();
                return;
            }
        }

        if( !dir.rename( old_path, i.name ) )
        {
            addLogWindow( "Ошибка перемещения файла" );
            finishProgramWithError();
            return;
        }

#if defined(Q_OS_LINUX)
        if( "./" + i.name == oconfig.getProgramName() )
        {
            QFile myfile( i.name );
            myfile.setPermissions(QFile::ExeUser);
        }
#endif
    }

    addLogWindow( "Обновление завершено" );
    finishProgram();
}

void MainWindow::setProgress( int value, int max )
{
    ui->progressBar->setFormat( QString("Файл %1 из %2").arg( value ).arg( max ) );        
    ui->progressBar->setValue( int((double)value/max * 100) );
}

QJsonDocument MainWindow::loadJson()
{
    QJsonDocument tmp;
    QFile jsonFile( QString( "%1%2%3" ).arg(LOCAL_PATH).arg("/").arg(INDEX_JSON) );

    if( jsonFile.open( QFile::ReadOnly ) == false )
        return tmp;

    tmp = QJsonDocument::fromJson( jsonFile.readAll() );
    jsonFile.close();

    return tmp;
}

QString MainWindow::getFileSHA1( const QString &fileName )
{
    QFile f(fileName);

    if( f.open(QFile::ReadOnly) )
    {
        QCryptographicHash hash( QCryptographicHash::Sha1 );

        if( hash.addData( &f ) )
        {
            return hash.result().toHex();
        }
    }

    return QString();
}

void MainWindow::finishProgramWithError()
{
    addLogWindow("Обновление завершилось с ошибками, нажмите [Продолжить]");
    ui->pushButton->setEnabled( true );
}

void MainWindow::finishProgram()
{
    QApplication::quit();

    // run external
    QString prg = oconfig.getProgramName();
    addLogWindow( "Запускаем программу " + prg );
    QProcess::startDetached( prg );
}

void MainWindow::on_pushButton_clicked()
{
    finishProgram();
}
