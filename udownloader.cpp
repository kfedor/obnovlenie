#include "udownloader.h"

UDownloader::UDownloader(QObject *parent) : QObject(parent)
{    
    connect( &nmanager, SIGNAL( finished(QNetworkReply*) ), this, SLOT( downloadFinished(QNetworkReply*) ) );
}

void UDownloader::setup( QString p_url, QString p_remote_path, QString p_login, QString p_password )
{
    url = p_url;
    remote_path = p_remote_path;
    login = p_login;
    password = p_password;
}

void UDownloader::downloadFile( QString filename )
{        
    QUrl myurl( url + remote_path + QString("/") + filename );
    //myurl.setUserInfo( login );
    //myurl.setPassword( password );

    QNetworkRequest request( myurl );
    QString slp = login + ":" + password;
    request.setRawHeader( QByteArray("Authorization"), QByteArray( "Basic " + slp.toUtf8().toBase64() ) );

    QNetworkReply *reply = nmanager.get( request );

    //connect( reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT( slotError(QNetworkReply::NetworkError)) );

#ifndef QT_NO_SSL
    connect( reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(sslErrors(QList<QSslError>)) );
#endif

    currentDownloads.append( reply );
}

void UDownloader::slotError( QNetworkReply::NetworkError err )
{
    emit fileError( "Сетевая ошибка" + err );
}

QString UDownloader::saveFileName( const QUrl &url )
{
    QDir dir = QDir::currentPath();

    if( !dir.exists( LOCAL_PATH ) )
        dir.mkdir( LOCAL_PATH );    

    QString path = url.path();

    path.remove( 0, remote_path.length() );

    dir.mkpath( LOCAL_PATH + QFileInfo(path).path() );

    //qDebug() << "Path: " << QFileInfo(path).path();
    //qDebug() << "Name: " << QFileInfo(path).fileName();

    return LOCAL_PATH + path;
}

bool UDownloader::saveToDisk( const QString &filename, QIODevice *data )
{
    QFile file(filename);

    if( !file.open(QIODevice::WriteOnly) )
    {
        fprintf( stderr, "Could not open %s for writing: %s\n", qPrintable(filename), qPrintable(file.errorString()) );
        return false;
    }

    file.write( data->readAll() );
    file.close();

    return true;
}


void UDownloader::downloadFinished( QNetworkReply *reply )
{    
    QUrl url = reply->url();

    if( reply->error() )
    {
        //fprintf( stderr, "Download of %s failed: %s\n", url.toEncoded().constData(), qPrintable(reply->errorString()) );
        //emit logMessage( QString("Сетевая ошибка при скачивании") + qPrintable( url ) + QString(":") + qPrintable(reply->errorString()) );
        //emit logMessage( qPrintable( url.toEncoded().constData() ));

        emit fileError( "Ошибка при скачивании: " + reply->errorString() );
        //emit fileError( "Ошибка при скачивании: " + url.host() + url.path() + QString(":") + "code [" + QString::number( reply->error() ) + "]" );
    }
    else
    {
        QString filename = saveFileName(url);

        if( saveToDisk(filename, reply) )
            //printf( "Download of %s succeeded (saved to %s)\n", url.toEncoded().constData(), qPrintable(filename) );
            //emit logMessage( QString("OK ") + qPrintable(filename) );
            emit fileComplete( filename );
    }

    currentDownloads.removeAll( reply );
    reply->deleteLater();
}

void UDownloader::sslErrors( const QList<QSslError> &sslErrors )
{
#ifndef QT_NO_SSL
    foreach (const QSslError &error, sslErrors)
        fprintf(stderr, "SSL error: %s\n", qPrintable(error.errorString()));
#else
    Q_UNUSED(sslErrors);
#endif
}
