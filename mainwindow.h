#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QFile>
#include <QCryptographicHash>
#include <QProcess>

#include "udownloader.h"
#include "oconfig.h"

#define INDEX_JSON "index.json"

#define VERSION "0.3"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    UDownloader ud;
    OConfig oconfig;

private:
    struct downloadData
    {
      QString name;
      QString size;
      QString sha1sum;
    };

    QList<downloadData> dlData;
    qint32 downloaded_count;
    Ui::MainWindow *ui;
    void parseIndexJson();
    QJsonDocument loadJson();
    void setProgress( int value, int max );
    QString getFileSHA1( const QString &fileName );
    void processDownloadItem( quint32 item_no );
    void moveDownloadedFiles();
    void finishProgram();
    void finishProgramWithError();

public slots:
    void addLogWindow( QString );
    void fileComplete( QString );
    void fileError( QString );

private slots:
    void on_pushButton_clicked();
};

#endif // MAINWINDOW_H
