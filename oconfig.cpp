#include "oconfig.h"

QString OConfig::chopString( QString in )
{
    if( in.endsWith("/"))
        in.chop(1);

    return in;
}

bool OConfig::loadSettings()
{       
    QSettings settings( O_CONFIG, QSettings::IniFormat );

    settings.beginGroup("settings");

    program_name = settings.value("program_name").toString();
    remote_path =  chopString( settings.value("remote_path").toString() );
    url = chopString( settings.value("url").toString() );
    login = settings.value("login").toString();
    password = settings.value("password").toString();

    settings.endGroup();

    if( program_name.length() == 0 )
        return false;

    if( url.length() == 0 )
        return false;

    if( program_name.length() == 0 )
        return false;
/*
    if( login.length() == 0 )
        return false;

    if( password.length() == 0 )
        return false;
*/
    return true;
}
